// ==UserScript==
// @name           LeakForums Group Owner Toolkit
// @version        2.0.1
// @description    Various tools for group owners to make managing groups easier.
// @downloadURL    https://bitbucket.org/notmike101/leakforums-group-manager-toolkit/raw/master/grouptoolkit.user.js
// @updateURL      https://bitbucket.org/notmike101/leakforums-group-manager-toolkit/raw/master/grouptoolkit.meta.js
//
// @author         Mike Orozco / IRDeNial
// @namespace      leakforums/irdenial
//
// @include        *://leakforums.net/managegroup.php?gid=*
// @include        *://www.leakforums.net/managegroup.php?gid=*
// @include        *://*.leakforums.net/managegroup.php?gid=*
//
// @grant          metadata
// @require        http://code.jquery.com/jquery-latest.js
// ==/UserScript==

/*
    Changelog
        v1.0.0 - 8/1/2014
            Initial Version
        v1.0.1 - 8/1/2014
            Added changelog
        v1.1.0 - 8/2/2014
            Added "Leaders" list
            Added Split view for UID list and Username list.
        v1.1.1 - 8/2/2014
            Hid default leaders list.
        v1.2 - 8/7/2014
            Added "Misc. Information" tool.
        v1.2.1 - 8/7/2014
            Removed "Misc. Information" tool.
            Fixed what I broke... :(
        v1.2.2 - 8/8/2014
            Fixed: https://github.com/IRDeNial/LSX-GroupToolkit/issues/1
        v1.2.3 - 8/16/2014
            Moved repositories
        v1.2.3a - 8/27/2014
            Moved back to original repo.  Fuck that -.-
        v1.2.3b - 9/9/2014
            Updated to include new domain.  No major changes.
        v2.0.0 - 8/16/2015
            Complete overhaul using newest jQuery & javascript functionality to ensure usability
            Change hosts to bitbucket
		v2.0.1 - 8/16/2015
			Fixed update URLs
*/

/**
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

(function ($) {
	var groupLeaders, users, i;
	groupLeaders = [];
	$('p:contains("Group Leaders") a').each(function () {
		var userID, userNm;
		userID = $(this).attr("href").split("/-")[1];
		userNm = $(this).text();
		groupLeaders.push([userNm,userID]);
	}).parent().hide();
	users = [];
	$('form:nth(0) table:nth(0) tr:nth-child(n+3)').each(function () {
		var userID, userNm, addUID, j;
		userID = $(this).children("td:first").children("a:first").attr("href").split("/-")[1];
		userNm = $(this).children("td:first").children("a:first").text();
		addUID = true;
		for(j = 0; j < groupLeaders.length; ++j) {
			if(groupLeaders[j][1] == userID) {
				addUID = false;
			}
		}
		if(addUID) {
			users.push([userNm,userID]);
		}
	});
	$('form:nth(0)').before('<table border="0" cellpadding="10" cellspacing="0" class="tborder"><tbody><tr><td class="thead" colspan="2"><strong>Group Leaders</strong></td></tr><tr><td class="trow1"><h3>Group Leader UID List</h3><textarea id="leaderIDList" readonly="readonly" style="width:90%;height:250px;"></textarea></td><td class="trow1"><h3>Group Leader Name List</h3><textarea id="leaderList" readonly="readonly" style="width:90%;height:250px;"></textarea></td></tr></tbody></table>');
	for(i = 0; i < groupLeaders.length; ++i) {
		$('#leaderIDList').text(
			$('#leaderIDList').text() + '[@' + groupLeaders[i][1] + ']' + '\n'
		);
	}
	for(i = 0; i < groupLeaders.length; ++i) {
		$('#leaderList').text(
			$('#leaderList').text() + groupLeaders[i][0] + '\n'
		);
	}
	$('form:nth(0)').before('<table border="0" cellpadding="10" cellspacing="0" class="tborder"><tbody><tr><td class="thead" colspan="2"><strong>Group Members</strong></td></tr><tr><td class="trow1"><h3>Group Member UID List</h3><textarea id="userIDList" readonly="readonly" style="width:90%;height:250px;"></textarea></td><td class="trow1"><h3>Group Member Name List</h3><textarea id="usernameList" readonly="readonly" style="width:90%;height:250px;"></textarea></td></tr></tbody></table>');

	for(i = 0; i < users.length; ++i) {
		$('#userIDList').text($('#userIDList').text() + '[@' + users[i][1] + ']' + '\n');
	}
	for(i = 0; i < users.length; ++i) {
		$('#usernameList').text($('#usernameList').text() + users[i][0] + '\n');
	}
})(jQuery);