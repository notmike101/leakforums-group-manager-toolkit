// ==UserScript==
// @name           LeakForums Group Owner Toolkit
// @version        2.0.1
// @description    Various tools for group owners to make managing groups easier.
// @downloadURL    https://bitbucket.org/notmike101/leakforums-group-manager-toolkit/raw/master/grouptoolkit.user.js
// @updateURL      https://bitbucket.org/notmike101/leakforums-group-manager-toolkit/raw/master/grouptoolkit.meta.js
//
// @author         Mike Orozco / IRDeNial
// @namespace      leakforums/irdenial
//
// @include        *://leakforums.net/managegroup.php?gid=*
// @include        *://www.leakforums.net/managegroup.php?gid=*
// @include        *://*.leakforums.net/managegroup.php?gid=*
//
// @grant          metadata
// @require        http://code.jquery.com/jquery-latest.js
// ==/UserScript==