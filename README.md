LSX-GroupToolkit
================
A toolkit to help group leaders manage their groups

## Install
https://bitbucket.org/notmike101/leakforums-group-manager-toolkit/raw/master/grouptoolkit.user.js

## Helping Out
Feel free to submit pull requests to be reviewed for new features.